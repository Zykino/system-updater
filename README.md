# Description
A tool to help keeping your system up to date.

This tool will remember the applications you installed and update them all (even from a git repo). To do that you will need to configure all the commands you want to run when upgrating your system (see the `example_config` folder).

See also [topgrade](https://github.com/topgrade-rs/topgrade) for a solution that auto-detect everything it can.

# Usage
The configuration folder is at [the standard path for configuration on your system](https://docs.rs/directories/latest/directories/struct.ProjectDirs.html#method.config_dir) and the commads should be in the packagers subfolder.
Example on linux it is at `~/.config/systemupdater/packagers/`

Just start the program, no option needed in you day to day usage.
```sh
sup
```

## Naming convention
Each packager file is executed in alphabetical order. To help maintain a coherent order of execution we use the following scheme: `<RANK>-<NAME>`.
Where `<RANK>` is a number corresponding to the moment the packager should be executed. And `<NAME>` the human name you want to call it.

The current default `<RANK>` are categorized as follow:
* We start at 5 and go 10 by 10 so you can easily add step in between (even if we do not expect that something may be more important to update than the system… Something related to system security maybe?)
* "05" is for updating the system’s standard package manager (`apt`, …)
* "15" is for other package managers (`flatpak`, …)
* "25" is for toolchain updater
* "35" is for language specific package manager
* "45" is for tools you update yourself with git
* No rank are executed last. *Depending on the implementation, this rank may disappear in the future*

**Note:** For now the ranks are only a suggestion, in the future we expect to be able to start all the one with the same `<RANK>` in parallel. Hence the space between steps: you may not want to run multiples CPU intensive packager in the same time.

## Installation
```sh
cargo install --locked system-updater
```

You will also need some configurations so the program knows what to execute. You can find the latest config examples [in the git repo](https://framagit.org/Zykino/system-updater/-/tree/master/example_packagers).
```sh
cd /tmp
curl -O "https://framagit.org/Zykino/system-updater/-/archive/master/system-updater-master.tar.gz?path=example_packagers"
tar -xzf system-updater-master.tar.gz\?path=example_packagers
# On most systems:
mv system-updater-master-example_packagers/example_packagers/ $HOME/.config/systemupdater/packagers
```

# Changelog
## In 0.1.0
* [X] Run commands found in different configuration files
* [X] Have Pre / Post commands to do before / after the upgrade
* [X] Do all the installations and give a summary of success and errors at the end
* [X] Improve errors ^^'
* [X] Executors from a packagers need to be executed in order and only if all of the previous failed
  * [X] Maybe test with `which` (the crate?) if the command exist before trying to execute it? (especially with admin rights)
* [ ] Documentation
  * [X] Readme

## Roadmap
This is unordered. Following my motivations and PR?
* [ ] Tests
* [ ] Add a `--no-continue -K` argument to stop at the first error
* [ ] Add options to show full config (all executor and if they can be used or not found), and computed config (with only the executors that will be used).
* [ ] Prevent the usage of `sudo` and equivalent, add a config to say "needAdminRights" or "execute as" == `sudo <username>`
* [ ] Parallel execution of commands
  * [ ] Show a progress bar general and the list of task in execution (with their stepNumber): "helix PostInstall (1/2)"
  * [ ] All input should be prevented (even asking for `sudo`’s password)
  * [ ] All output (standard and errors) needs to be redirected in files. Maybe something like: /tmp/system-updater/<DATETIME>-<packagerName>-<executorName?/stepNumber??>.log
    * [ ] I think having a single file per command or executor with stdout & stderr is the best, maybe prefixing each line with "stdout: "
    * [ ] A temporary folder is nice for humans inspecting immediately the logs, but we may want a way to archivate logs (at least propose an option to choose an other destination folder)
* [ ] Execute on a distant computer 
  * [ ] Have the sames subtasks as parallel execution for the UI
  * [ ] Retrive the logs from distant machines (both OK and err)
* [ ] Documentation
  * [ ] Code
* [ ] Add CI
* [ ] Find a way to also distribute the "example_packagers" config folder. Using [cargo parcel](https://gitlab.com/rotty/cargo-parcel)? A `--dump` option? Something else?

## Nice to have
* [ ] Being usable as a lib (I set the minimum possible in main.rs but the insterface have not been thought out)
  * [ ] Being given a directory and/or a list of files
  * [ ] Being given an already populated `Updater`’s struct
* [ ] Have subcommands to interactively act on packagers’s config
  * [ ] List
  * [ ] Show
  * [ ] Create
  * [ ] Delete
  * [ ] Update? (Bad way: Show + Delete + Create? ^^')
* [ ] More config examples
  * [ ] System executors for all/most famous systems (openSUSE, Fedora, Arch, …)
  * [ ] At least an example of binary downlowd (curl or wget)
* [ ] Start itself and sub-command with a "nicer" ressource usage (Not sure it is possible in all systems, or in the same way)
