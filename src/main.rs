use system_updater::{command::UpdateSteps, *};

use clap::Parser;

fn main() -> std::io::Result<()> {
    let mut opt = Opt::parse();

    if opt.steps.is_empty() {
        opt.steps = vec![
            UpdateSteps::PreInstall,
            UpdateSteps::Install,
            UpdateSteps::PostInstall,
        ]
    };

    let summary = match run(&opt) {
        Ok(s) => format!("{}", s),
        Err(e) => format!("{}", e),
    };

    println!("{}", summary);
    Ok(())
}
